package edu.polo.coatic.controladores;

import edu.polo.coatic.entidades.*;
import edu.polo.coatic.servicios.*;
import jakarta.validation.Valid;
import java.io.File;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("cursos")
public class CursoControlador implements WebMvcConfigurer {

  @Autowired
  AreaServicio areaServicio;

  @Autowired
  CursoServicio cursoServicio;

  @GetMapping
  public ModelAndView index() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("fragments/base");
    mav.addObject("titulo", "Listado de cursos");
    mav.addObject("vista", "cursos/index");
    mav.addObject("cursos", cursoServicio.getAll());
    return mav;
  }

  @GetMapping("/crear")
  public ModelAndView crear(Curso curso) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("fragments/base");
    mav.addObject("titulo", "Crear curso");
    mav.addObject("vista", "cursos/crear");
    mav.addObject("curso", curso);
    mav.addObject("areas", areaServicio.getAll());
    return mav;
  }

  @PostMapping("/crear")
  public ModelAndView guardar(@RequestParam("archivo") MultipartFile archivo, @Valid Curso curso, BindingResult br, RedirectAttributes ra) {
    if ( archivo.isEmpty() )
      br.reject("archivo", "Por favor, cargue una imagen");

    if ( br.hasErrors() ) {
      return this.crear(curso);
    }
    cursoServicio.save(curso);

    String tipo = archivo.getContentType();
    String extension = "." + tipo.substring(tipo.indexOf('/') + 1, tipo.length());
    String imagen = curso.getId() + extension;
    String path = Paths.get("src/main/resources/static/images/cursos", imagen).toAbsolutePath().toString();
    ModelAndView mav = this.index();

    try {
      archivo.transferTo( new File(path) );
    } catch (Exception e) {
      mav.addObject("error", "No se pudo guardar la imagen");
      return mav;
    }

    curso.setImagen(imagen);
    cursoServicio.save(curso);
    mav.addObject("exito", "Curso creado exitosamente");
    return mav;
  }

	@GetMapping("/editar/{id}")
  public ModelAndView editar(@PathVariable("id") Long id, Curso curso)
  {
    return this.editar(id, curso, true);
  }

  public ModelAndView editar(@PathVariable("id") Long id, Curso curso, boolean estaPersistido) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("fragments/base");
    mav.addObject("titulo", "Editar curso");
    mav.addObject("vista", "cursos/editar");
    mav.addObject("curso", cursoServicio.getById(id));
    mav.addObject("areas", areaServicio.getAll());

    if (estaPersistido)
      mav.addObject("curso", cursoServicio.getById(id));
    else
      curso.setImagen( cursoServicio.getById(id).getImagen() );

    return mav;
  }

  @PutMapping("/editar/{id}")
  private ModelAndView update(@PathVariable("id") Long id,
  @RequestParam(value = "archivo", required = false) MultipartFile archivo,
  @Valid Curso curso, BindingResult br, RedirectAttributes ra) {
    if ( br.hasErrors() ) {
      return this.editar(id, curso, false);
    }

    Curso registro = cursoServicio.getById(id);
    registro.setNombre( curso.getNombre() );
    registro.setMeses( curso.getMeses() );
    registro.setInicio( curso.getInicio() );
    registro.setPresencial( curso.isPresencial() );
    registro.setArea( curso.getArea() );
    ModelAndView maw = this.index();

    if ( ! archivo.isEmpty() ) {
      String tipo = archivo.getContentType();
      String extension = "." + tipo.substring(tipo.indexOf('/') + 1, tipo.length());
      String imagen = curso.getId() + extension;
      String path = Paths.get("src/main/resources/static/images/cursos", imagen).toAbsolutePath().toString();

      try {
        archivo.transferTo( new File(path) );
      } catch (Exception e) {
        maw.addObject("error", "No se pudo guardar la imagen");
        return maw;
      }

        registro.setImagen(imagen);
    }

    cursoServicio.save(registro);
    maw.addObject("exito", "Curso editado exitosamente");
    return maw;
  }

  @DeleteMapping("/{id}")
  public ModelAndView eliminar(@PathVariable("id") Long id) {
    cursoServicio.delete(id);
    ModelAndView mav = this.index();
    mav.addObject("exito", "Curso eliminado exitosamente");
    return mav;
  }
}
