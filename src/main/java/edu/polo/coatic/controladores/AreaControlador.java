package edu.polo.coatic.controladores;

import edu.polo.coatic.entidades.*;
import edu.polo.coatic.servicios.*;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("areas")
public class AreaControlador implements WebMvcConfigurer {

  @Autowired
  AreaServicio areaServicio;

  @GetMapping
  public ModelAndView index() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("fragments/base");
    mav.addObject("titulo", "Listado de áreas");
    mav.addObject("vista", "areas/index");
    mav.addObject("areas", areaServicio.getAll());
    return mav;
  }

  @GetMapping("/crear")
  public ModelAndView crear(Area area) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("fragments/base");
    mav.addObject("titulo", "Crear área");
    mav.addObject("vista", "areas/crear");
    mav.addObject("area", area);
    return mav;
  }

  @PostMapping("/crear")
  public ModelAndView guardar(@Valid Area area, BindingResult br, RedirectAttributes ra) {
    if ( br.hasErrors() ) {
      return this.crear(area);
    }

    areaServicio.save(area);

    ModelAndView mav = this.index();
    mav.addObject("exito", "Área creada exitosamente");
    return mav;
  }

  @GetMapping("/editar/{id}")
  public ModelAndView editar(@PathVariable("id") Long id, Area area) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("fragments/base");
    mav.addObject("titulo", "Editar área");
    mav.addObject("vista", "areas/editar");
    mav.addObject("area", areaServicio.getById(id));

    return mav;
  }

  @PutMapping("/editar/{id}")
  public ModelAndView actualizar(@PathVariable("id") Long id, @Valid Area area, BindingResult br, RedirectAttributes ra) {
    if ( br.hasErrors() ) {
      ModelAndView mav = new ModelAndView();
      mav.setViewName("fragments/base");
      mav.addObject("titulo", "Editar área");
      mav.addObject("vista", "areas/editar");
      mav.addObject("area", area);
      return mav;
    }

    Area registro = areaServicio.getById(id);
    registro.setNombre(area.getNombre());
    ModelAndView mav = this.index();

    areaServicio.save(registro);
    mav.addObject("exito", "Área editada exitosamente");
    return mav;
  }

  @DeleteMapping("/{id}")
  public ModelAndView eliminar(@PathVariable("id") Long id) {
    ModelAndView mav = this.index();

    Area area = areaServicio.getById(id);

    if (area.getCursos().isEmpty()) {
      areaServicio.delete(id);
      mav.addObject("exito", "Área eliminada exitosamente");
    } else {
      mav.addObject("error", "No se puede borrar el registro porque posee datos asociados");
    }

    return mav;
  }
}
